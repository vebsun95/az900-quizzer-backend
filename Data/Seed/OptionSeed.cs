using az900_quizzer.Models;

namespace az900_quizzer.Data.Seed;

public class OptionSeed
{
    public static ICollection<Option> GetOptions()
    { 
        return new Option[]
        {
            new Option()
                {
                    Id=1,
                    Value="True",
                    IsRight=false,
                    QuestionId=1,
                },
            new Option()
                {
                    Id=2,
                    Value="False",
                    IsRight=true,
                    QuestionId=1,
                },
            new Option()
                {
                    Id=3,
                    Value="Delivery of computing services over the internet.",
                    IsRight=true,
                    QuestionId=2,
                },
            new Option()
                {
                    Id=4,
                    Value="Setting up your own datacenter.",
                    IsRight=false,
                    QuestionId=2,
                },
            new Option()
                {
                    Id=5,
                    Value="Using the internet",
                    IsRight=false,
                    QuestionId=2,
                },
            new Option()
                {
                    Id=6,
                    Value="Faster innovation",
                    IsRight=false,
                    QuestionId=3,
                },
            new Option()
                {
                    Id=7,
                    Value="A limited pool of services",
                    IsRight=true,
                    QuestionId=3,
                },
            new Option()
                {
                    Id=8,
                    Value="Speech recognition and other cognitive services",
                    IsRight=false,
                    QuestionId=3,
                },
            new Option()
                {
                    Id=9,
                    Value="Azure initiatives",
                    IsRight=false,
                    QuestionId=4,
                },
            new Option()
                {
                    Id=10,
                    Value="Management groups",
                    IsRight=true,
                    QuestionId=4,
                },
            new Option()
                {
                    Id=11,
                    Value="Resource groups",
                    IsRight=false,
                    QuestionId=4,
                },
            new Option()
                {
                    Id=12,
                    Value="Azure subscription",
                    IsRight=true,
                    QuestionId=5,
                },
            new Option()
                {
                    Id=13,
                    Value="Management group",
                    IsRight=false,
                    QuestionId=5,
                },
            new Option()
                {
                    Id=14,
                    Value="Resource group",
                    IsRight=false,
                    QuestionId=5,
                },
            new Option()
                {
                    Id=15,
                    Value="Public cloud",
                    IsRight=false,
                    QuestionId=5,
                },
            new Option()
                {
                    Id=16,
                    Value="Resources can be in only one resource group.",
                    IsRight=false,
                    QuestionId=6,
                },
            new Option()
                {
                    Id=17,
                    Value="Role-based access control can be applied to the resource group.",
                    IsRight=false,
                    QuestionId=6,
                },
            new Option()
                {
                    Id=18,
                    Value="Resource groups can be nested.",
                    IsRight=true,
                    QuestionId=6,
                },
            new Option()
                {
                    Id=19,
                    Value="Using Azure doesn't require a subscription.",
                    IsRight=false,
                    QuestionId=7,
                },
            new Option()
                {
                    Id=20,
                    Value="An Azure subscription is a logical unit of Azure services.",
                    IsRight=true,
                    QuestionId=7,
                },
        };
    }
}