using az900_quizzer.Models;

namespace az900_quizzer.Data.Seed;

public class QuizSeed
{
    public static ICollection<Quiz> GetQuizzes()
    {
        return new Quiz[]
        {
            new Quiz()
                {
                    Id=1,
                    Name="Fundementals",
                    Uploaded=DateTime.Now,
                },
            new Quiz()
                {
                    Id=2,
                    Name="Core Azure Achitecrual Components",
                    Uploaded=DateTime.Now,
                },
        };
    }
}