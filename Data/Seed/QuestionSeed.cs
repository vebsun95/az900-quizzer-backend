using az900_quizzer.Models;

namespace az900_quizzer.Data.Seed;

public class QuestionSeed
{
    public static ICollection<Question> GetQuestions()
    {
        return new Question[]
        {
            new Question()
                {
                    Id=1,
                    Value="True or false: You need to purchase an Azure account before you can use any Azure resources.",
                    QuizId=1,
                },
            new Question()
                {
                    Id=2,
                    Value="What is meant by cloud computing?",
                    QuizId=1,
                },
            new Question()
                {
                    Id=3,
                    Value="Which of the following is not a feature of Cloud computing?",
                    QuizId=1,
                },
            new Question()
                {
                    Id=4,
                    Value="Which of the following can be used to manage governance across multiple Azure subscriptions?",
                    QuizId=2,
                },
            new Question()
                {
                    Id=5,
                    Value="Which of the following is a logical unit of Azure services that links to an Azure account?",
                    QuizId=2,
                },
            new Question()
                {
                    Id=6,
                    Value="Which of the following features does not apply to resource groups?",
                    QuizId=2,
                },
            new Question()
                {
                    Id=7,
                    Value="Which of the following statements is a valid statement about an Azure subscription",
                    QuizId=2,
                },
        };
    }
}