using az900_quizzer.Models;
using Microsoft.EntityFrameworkCore;
using az900_quizzer.Data.Seed;

namespace az900_quizzer.Data;

public class AZ900QuizzerDbContext : DbContext
{
    #nullable disable
    public DbSet<Quiz> Quizzes {get; set;}
    public DbSet<Question> Questions {get; set;}
    public DbSet<Option> Options {get; set;}
    #nullable restore

    public AZ900QuizzerDbContext(DbContextOptions<AZ900QuizzerDbContext> options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Quiz>()
            .HasData(QuizSeed.GetQuizzes());
        modelBuilder.Entity<Question>()
            .HasData(QuestionSeed.GetQuestions());
        modelBuilder.Entity<Option>()
            .HasData(OptionSeed.GetOptions());
    }
}