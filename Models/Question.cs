using System.ComponentModel.DataAnnotations;

namespace az900_quizzer.Models;

public class Question
{
    [Key]
    public int Id {get; set;}
    [MaxLength(256)]
    public string Value {get; set;}

    public Quiz Quiz {get; set;} 
    public int QuizId {get; set;}
    public ICollection<Option> Options {get; set;}
}