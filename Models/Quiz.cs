using System.ComponentModel.DataAnnotations;

namespace az900_quizzer.Models;

public class Quiz
{
    [Key]
    public int Id {get; set;}
    [MaxLength(64)]
    public string Name {get; set;}
    public DateTime Uploaded {get; set;}
    public ICollection<Question> Questions {get; set;}
}