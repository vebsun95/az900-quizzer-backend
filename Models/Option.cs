using System.ComponentModel.DataAnnotations;

namespace az900_quizzer.Models;

public class Option
{
    [Key]
    public int Id {get; set;}
    [MaxLength(256)]
    public string Value {get; set;}
    public bool IsRight {get; set;}

    public Question Question {get; set;}
    public int QuestionId {get; set;}
}