using az900_quizzer.DTOs.OptionDTOs;

namespace az900_quizzer.DTOs.QuestionDTOs;

public class QuestionReadDTO
{
    public int Id {get; set;}
    public string Value {get; set;}
    public ICollection<OptionReadDto> Options {get; set;}
}