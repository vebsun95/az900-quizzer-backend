namespace az900_quizzer.DTOs.OptionDTOs;

public class OptionReadDto
{
    public int Id {get; set;}
    public string Value {get; set;}
    public bool IsRight {get; set;}
}