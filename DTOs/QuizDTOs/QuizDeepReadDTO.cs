using az900_quizzer.DTOs.QuestionDTOs;

namespace az900_quizzer.DTOs.QuizDTOs;

public class QuizDeepReadDTO
{
    public int Id {get; set;}
    public string Name {get; set;}
    public ICollection<QuestionReadDTO> Questions {get; set;}
}