namespace az900_quizzer.DTOs.QuizDTOs;

public class QuizSimpleReadDTO
{
    public int Id {get; set;}
    public string Name {get; set;}
}