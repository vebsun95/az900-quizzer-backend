using AutoMapper;
using az900_quizzer.DTOs.OptionDTOs;
using az900_quizzer.Models;

namespace az900_quizzer.Profiles;

public class OptionProfile : Profile
{
    public OptionProfile()
    {
        CreateMap<Option, OptionReadDto>();
    }
}