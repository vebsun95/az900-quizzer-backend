using AutoMapper;
using az900_quizzer.DTOs.QuizDTOs;
using az900_quizzer.Models;

namespace az900_quizzer.Profiles;

public class QuizProfile : Profile
{
    public QuizProfile()
    {
        CreateMap<Quiz, QuizDeepReadDTO>();
        CreateMap<Quiz, QuizSimpleReadDTO>();
    }
}