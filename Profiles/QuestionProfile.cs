using AutoMapper;
using az900_quizzer.DTOs.QuestionDTOs;
using az900_quizzer.Models;

namespace az900_quizzer.Profiles;

public class QuestionProfile : Profile
{
    public QuestionProfile()
    {
        CreateMap<Question, QuestionReadDTO>();
    }
}