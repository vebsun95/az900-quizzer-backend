using az900_quizzer.Data;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<AZ900QuizzerDbContext>(options => 
{
    string connectionString = builder.Environment.IsDevelopment() ?
        builder.Configuration.GetConnectionString("dev") :
        builder.Configuration.GetConnectionString("default");
    var serverVersion = ServerVersion.AutoDetect(connectionString);
    options.UseMySql(connectionString, serverVersion)
    .LogTo(Console.WriteLine, LogLevel.Information)
    .EnableSensitiveDataLogging()
    .EnableDetailedErrors();;
});

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


app.UseHttpsRedirection();

app.UseAuthorization();
app.Use( async (context, next) => {
    await next();
    var path = context.Request.Path.Value;
    if(!path.StartsWith("/api") && !Path.HasExtension(path))
    {
        context.Request.Path = "/index.html";
        await next();
    }
});
app.UseStaticFiles();

app.MapControllers();



app.Run();
