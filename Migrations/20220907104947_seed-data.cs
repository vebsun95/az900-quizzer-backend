﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace az900_quizzer.Migrations
{
    public partial class seeddata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Quizzes",
                columns: new[] { "Id", "Name", "Uploaded" },
                values: new object[] { 1, "Fundementals", new DateTime(2022, 9, 7, 12, 49, 47, 46, DateTimeKind.Local).AddTicks(8974) });

            migrationBuilder.InsertData(
                table: "Quizzes",
                columns: new[] { "Id", "Name", "Uploaded" },
                values: new object[] { 2, "Core Azure Achitecrual Components", new DateTime(2022, 9, 7, 12, 49, 47, 46, DateTimeKind.Local).AddTicks(9015) });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "QuizId", "Value" },
                values: new object[,]
                {
                    { 1, 1, "True or false: You need to purchase an Azure account before you can use any Azure resources." },
                    { 2, 1, "What is meant by cloud computing?" },
                    { 3, 1, "Which of the following is not a feature of Cloud computing?" },
                    { 4, 2, "Which of the following can be used to manage governance across multiple Azure subscriptions?" },
                    { 5, 2, "Which of the following is a logical unit of Azure services that links to an Azure account?" },
                    { 6, 2, "Which of the following features does not apply to resource groups?" },
                    { 7, 2, "Which of the following statements is a valid statement about an Azure subscription" }
                });

            migrationBuilder.InsertData(
                table: "Options",
                columns: new[] { "Id", "IsRight", "QuestionId", "Value" },
                values: new object[,]
                {
                    { 1, false, 1, "True" },
                    { 2, true, 1, "False" },
                    { 3, true, 2, "Delivery of computing services over the internet." },
                    { 4, false, 2, "Setting up your own datacenter." },
                    { 5, false, 2, "Using the internet" },
                    { 6, false, 3, "Faster innovation" },
                    { 7, true, 3, "A limited pool of services" },
                    { 8, false, 3, "Speech recognition and other cognitive services" },
                    { 9, false, 4, "Azure initiatives" },
                    { 10, true, 4, "Management groups" },
                    { 11, false, 4, "Resource groups" },
                    { 12, true, 5, "Azure subscription" },
                    { 13, false, 5, "Management group" },
                    { 14, false, 5, "Resource group" },
                    { 15, false, 5, "Public cloud" },
                    { 16, false, 6, "Resources can be in only one resource group." },
                    { 17, false, 6, "Role-based access control can be applied to the resource group." },
                    { 18, true, 6, "Resource groups can be nested." },
                    { 19, false, 7, "Using Azure doesn't require a subscription." },
                    { 20, true, 7, "An Azure subscription is a logical unit of Azure services." }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Options",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Quizzes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Quizzes",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
