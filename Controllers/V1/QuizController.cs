using Microsoft.AspNetCore.Mvc;
using az900_quizzer.Data;
using az900_quizzer.Models;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using az900_quizzer.DTOs.QuizDTOs;
using System.Net.Mime;

namespace az900_quizzer.Controllers;

[ApiController]
[Route("api/v1/quiz")]
[Produces(MediaTypeNames.Application.Json)]
[Consumes(MediaTypeNames.Application.Json)]
public class QuizController : ControllerBase
{

    private readonly ILogger<QuizController> _logger;
    private readonly AZ900QuizzerDbContext _context;
    private readonly IMapper _mapper;

    public QuizController(ILogger<QuizController> logger, AZ900QuizzerDbContext context, IMapper mapper)
    {
        _logger = logger;
        _context = context;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<IEnumerable<QuizSimpleReadDTO>> Get()
    {
        var quizzes = await _context.Quizzes.ToListAsync();
        return quizzes.Select(quiz => _mapper.Map<Quiz, QuizSimpleReadDTO>(quiz));
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<QuizDeepReadDTO>> GetById(int id)
    {
        Quiz? quiz = await _context.Quizzes
            .Include(quiz => quiz.Questions)
                .ThenInclude(question => question.Options)
            .FirstOrDefaultAsync(q => q.Id == id);
        if(quiz is null)
            return NotFound();
        return _mapper.Map<Quiz, QuizDeepReadDTO>(quiz);
    }
}